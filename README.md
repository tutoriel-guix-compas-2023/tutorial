# Guix tutorial - Compas 2023: Tutorial

[![pipeline status](https://gitlab.inria.fr/tutoriel-guix-compas-2023/tutorial/badges/master/pipeline.svg)](https://gitlab.inria.fr/tutoriel-guix-compas-2023/tutorial/-/commits/master)

Towards a reproducible research study with Guix

**[Tutorial](https://tutoriel-guix-compas-2023.gitlabpages.inria.fr/tutorial/)**
